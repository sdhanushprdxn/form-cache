const express = require('express');
const bodyParse = require('body-parser');
const cookieParse = require('cookie-parser');
const session = require('express-session')
const app = express();

app.use(cookieParse())
app.use(session({secret:'secretkey' , saveUninitialized:true , resave:true , cookie:{maxage:6000}}));
app.use(bodyParse.urlencoded({extended:true}));

//redirecting to the form with get.
app.get('/' ,(req,res) => {
  res.redirect('/form-with-get');
});


app.get('/form-with-get' ,(req,res) => {
  res.sendFile(__dirname + '/html/form-with-get.html');
  // req.session.views++;
  // res.send('visted' + req.session.views + 'times')
});

app.get('/form-with-post' ,(req,res) => {
  res.sendFile(__dirname + '/html/form-with-post.html');
});

app.get('/submit-form-with-get',(req,res) => {
  res.cookie(req.query.first,req.query.last).send(req.query);
  // res.send(req.query.first);
})

app.post('/submit-form-with-post',(req,res) => {
  res.send(req.body);
  // res.send(req.body.first);
})
app.listen(3000);